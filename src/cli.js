import arg from 'arg';
import inquirer from 'inquirer';
import logs from './logs'
import process from '../config/process';
import context from '../config/context';
function parseArgumentsIntoOptions(rawArgs) {
    const args = arg(
        {
            '--git': Boolean,
            '--yes': Boolean,
            '--process': String,
            '-g': '--git',
            '-y': '--yes',
            '-p': '--process',
        },
        {
            argv: rawArgs.slice(2),
        }
    );
    return {
        context: args._[0],
        process: args['--process'],
        skipPrompts: args['--yes'] || false,
        git: args['--git'] || false,
        runInstall: args['--install'] || false,
    };
}


function promptForMissingOptions(options) {

    return new Promise(function (resolve, reject) {

        const defaultTemplate = 'logs';
        const questions = [];

        if (!context.includes(options.context) || !options.context) {
            questions.push({
                type: 'list',
                name: 'context',
                message: 'Please specify the context to proceed',
                choices: context,
                default: defaultTemplate,
            });
        }

        if (!options.process || !process.includes(options.process)) {
            questions.push({
                type: 'list',
                when: function (response) {
                    return response.context != 'stat';
                },
                name: 'process',
                message: 'Please specify the process',
                choices: process,
            });
        }

        var answers = {}
        inquirer.prompt(questions).then(function (success) {
            answers = success;
            options['context'] = options.context || answers.context;
            options['process'] = options.process || answers.process;
            return resolve(options);
        }).catch(function (error) {
            return reject(error)
        })

    });

}

export function cli(args) {

    return new Promise(function (resolve, reject) {
        let options = parseArgumentsIntoOptions(args);
        promptForMissingOptions(options).then(function (options) {
            switch (options['context']) {
                case 'logs':
                    process(logs(options['process']));
                    break;
                case 'stat':
                    console.log('stat');
                default:
                    break;
            }
        });
    });

}



function process(context) {
    context.then(function () {
        
    }).catch(function (error) {
        console.error(error);
    })
}

module.exports = cli;