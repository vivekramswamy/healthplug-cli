var fs = require('fs')
var moment = require('moment');
var path = require('path');
var CronJob = require('cron').CronJob;
var dirPath ,fileName;

function Logger(dirPath, fileName) {
    this.dirPath = dirPath;
    this.fileName = fileName;

}

Logger.prototype.rotate = function () {
    const file = dirPath + fileName + '.' + moment().format('DDMMYYYY') + '.log'
    access = fs.createWriteStream(file, { flags: 'a' });
    process.stdout.write = process.stderr.write = access.write.bind(access);
}


Logger.prototype.report = function (dirPath, fileName) {
    var wstream = fs.createWriteStream(dirPath + 'healthplug.log', { flags: 'a' });
    wstream.write(fileName.toString().toUpperCase() + '\t' + moment().format('DD MMM YYYY hh:mm:ss a'));
    wstream.write('\n')
    wstream.end();
}


module.exports = function (d, i) {
    var logger = new Logger(d, i);
    const rotateJob = new CronJob('0 0 0 * * *', logger.rotate);
    dirPath =d;
    fileName = i;
    logger.report(d, i);;
    logger.rotate();
    rotateJob.start();
};

