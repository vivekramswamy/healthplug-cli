const fs = require('fs')
const _process = require('process');
var moment = require('moment');
Tail = require('tail').Tail;
const DIR = _process.env.HEALTHPLUG_WRK_DIR;
function logs(process) {

    const path = DIR + '/logs/' + process + '.' + moment().format('DDMMYYYY') + '.log'
    console.log(path);

    try {
        if (!fs.existsSync(path))
            return tail(null);
        return tail(path);
    } catch (err) {
        console.log('Error in fetching current log file', err);
    }
}


function tail(path) {

    return new Promise(function (resolve,reject) {

        if(!path)
            return reject('File not found');

        tail = new Tail(path);

        tail.on("line", function (data) {
            console.log(data);
        });
    
        tail.on("error", function (error) {
            console.log('Error reading log', error);
            tail.unwatch();
            return reject(error);
        });
    });
   

}
module.exports = logs;